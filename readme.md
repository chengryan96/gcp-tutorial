# Google Cloud Platform setup tutorial

This is a tutorial for setting up a virtual machine (VM) in Google Cloud Platform (GCP).


### Setup a VM in GCP

Please refer to [creating a virtual machine instance](https://cloud.google.com/compute/docs/quickstart-linux#create_a_virtual_machine_instance).

**Please note that not all regions provide GPUs**, for details, please refer to [Creating an instance with one or more GPUs](https://cloud.google.com/ai-platform/deep-learning-vm/docs/cloud-marketplace#creating_an_instance_with_one_or_more_gpus).

Using [Deep Learning VM Image](https://cloud.google.com/deep-learning-vm) will automatically install Cuda and Tensorflow for you, 

### Connecting VSCode to GCP

##### For Windows:

1. Startup *Windows Powershell*

2. following the remaining step of ***'For Linux and MACOS'***

   Please remove the below syntax whereever it appears. 

   ```sh
   ~/.ssh/
   ```

   For example,

   In Linux and MACOS the syntax will be

   ```sh
   ssh-keygen -t rsa -f ~/.ssh/[KEY_FILENAME] -C [USERNAME]
   ```

   In windows, type the below syntax instead

   ```sh
   ssh-keygen -t rsa -f [KEY_FILENAME] -C [USERNAME]
   ```

   

##### For Linux and MACOS:
1. Setup SSH keys on your ***local computer*** following the tutorial [creating a new SSH key](https://cloud.google.com/compute/docs/instances/adding-removing-ssh-keys#createsshkeys).

2. Locate your SSH key following the tutorial [Locating an SSH key](https://cloud.google.com/compute/docs/instances/adding-removing-ssh-keys#locatesshkeys).

3. Add your ***local SSH key*** to your GCP account.  
Refer to [Adding or Adding or removing project-wide public SSH keys](https://cloud.google.com/compute/docs/instances/adding-removing-ssh-keys#project-wide) if you want to let this machine access to all VM(s) in this project.
Refer to [Adding or removing instance-level public SSH keys](https://cloud.google.com/compute/docs/instances/adding-removing-ssh-keys#instance-only) if you want to let this machine access to a single VM.
Please note the the public SSH key is in the format *[Key-name].pub* .

4. Open VSCode on your local computer. Open the *Externsion manager* in VSCode by pressing *(Ctrl/Cmd)+Shift+x* and install *remote-ssh*.

5. Press *(Ctrl/Cmd)+shift+p* or clicking the green box under the lower left hand corner to open the Command Palette  on VSCode and type *remote-ssh:connect to host*, and then choose the *Add new ssh host* option. 

6. On the *Enter SSH Connection command* type 

   ```
   ssh -i ~/.ssh/[KEY_FILENAME] [USERNAME]@[External IP]
   ```

   *KEY_FILENAME* and *USERNAME* can be found on [creating a new SSH key](https://cloud.google.com/compute/docs/instances/adding-removing-ssh-keys#createsshkeys).

   For *External IP* please refer to the picture below.

   

   ![Screenshot 2020-07-23 at 15.34.45](https://gitlab.com/chengryan96/gcp-tutorial/-/raw/master/pic/Screenshot%202020-07-23%20at%2015.34.45.png)

   

   ***For Windows Users***

   Please type the full path with \\\ as below

   ```sh
   ssh -i C:\\Users\\your-windows-username\\[KEY_FILENAME] [USERNAME]@[External IP]
   ```

   

7. Choose the first option as the figure shown below.

![Screenshot 2020-07-23 at 15.42.15](https://gitlab.com/chengryan96/gcp-tutorial/-/raw/master/pic/Screenshot%202020-07-23%20at%2015.42.15.png)

   If you do it correctly the lower right hand corner will show the box as the figure shown below.

![Screenshot 2020-07-23 at 15.46.42](https://gitlab.com/chengryan96/gcp-tutorial/-/raw/master/pic/Screenshot%202020-07-23%20at%2015.46.42.png)

8. Open the Command Paletter and type *remote-ssh:connect to host* again. Connect to the host you want.  

![Screenshot 2020-07-23 at 15.49.46](https://gitlab.com/chengryan96/gcp-tutorial/-/raw/master/pic/Screenshot%202020-07-23%20at%2015.49.46.png)
   
   you can check your lower left hand corner to see if you have done it correctly or not.

![Screenshot 2020-07-23 at 15.52.21](https://gitlab.com/chengryan96/gcp-tutorial/-/raw/master/pic/Screenshot%202020-07-23%20at%2015.52.21.png)

### Share president disk between VMs

1. Go to the disk option and create the disk.

2. please refer to [Sharing a regional persistent disk between multiple instances](https://cloud.google.com/compute/docs/disks/regional-persistent-disk#use_multi_instances)

   ***Please note that the disk can only be read-only***

